import { NgModule } from '@angular/core';
import { LayoutViewModule } from './components/layout/layout.module';

@NgModule({
  imports: [LayoutViewModule],
  exports: [LayoutViewModule],
})
export class LayoutModule {}

