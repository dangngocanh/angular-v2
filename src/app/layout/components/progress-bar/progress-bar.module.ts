import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { ProgressBarComponent } from './progress-bar.component';
import { ProgressBarService } from './progress-bar.service';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
    declarations: [
       ProgressBarComponent
    ],
    imports     : [
        CommonModule,
        HttpClientModule,
        RouterModule,
        SharedModule
    ],
    exports     : [
        ProgressBarComponent,

    ],
})
export class ProgressBarModule
{
}
