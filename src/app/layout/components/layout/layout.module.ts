import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

import { LayoutComponent } from './layout.component';
import { LayoutService } from './layout.service';
import { SidebarComponent } from './sidebar/sidebar.component';

@NgModule({
  declarations: [
    LayoutComponent,
    SidebarComponent
  ],
  imports: [
    RouterModule,
    SharedModule,
  ],
  exports: [LayoutComponent],
  providers: [
    LayoutService
  ]

})
export class LayoutViewModule {}
