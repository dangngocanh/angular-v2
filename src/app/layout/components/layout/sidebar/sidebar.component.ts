import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  trigger,
  transition,
  style,
  animate,
  state,
} from '@angular/animations';
import { LayoutService } from '../layout.service';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  animations: [
    trigger(
      'slide', [
        state('collapse', style({
          width: 0,
          opacity: 0
        })),
        state('thin', style({
          width: '50%',
          opacity: 1
        })),
        state('full', style({
          width: '100%',
          opacity: 1
        })),
        transition('collapse => thin', animate('100ms ease-in')),
        transition('thin => full', animate('100ms ease-in')),
        transition('full => collapse', animate('100ms ease-out'))
      ]
    )
  ],
})
export class SidebarComponent implements OnInit, OnDestroy {
  private sub: Subscription;
  state: string;
  constructor(public layout: LayoutService) {}

  ngOnInit(): void {
    this.sub = this.layout.state$.subscribe((s: string) => {
      console.log(s);
      return this.state = s
    });
  }
  ngOnDestroy() {
    this.sub && this.sub.unsubscribe();
  }
}
