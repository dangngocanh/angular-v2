import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpServerService {
  private REST_API_SERVICE =  "http://localhost:3000";
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }
  constructor(private httpClient: HttpClient) { }

  getProfile(): Observable<any> {
    let url = this.REST_API_SERVICE +"/profile";
    return this.httpClient.get(url,this.httpOptions).pipe()
  }
  getList(): Observable<any> {
    let url = this.REST_API_SERVICE +"/posts";
    return this.httpClient.get(url,this.httpOptions).pipe()
  }
  addPost(data): Observable<any> {
    let url = this.REST_API_SERVICE +"/posts";
    return this.httpClient.post(url,data,this.httpOptions).pipe()
  }
  updatePost(data,id): Observable<any> {
    let url = this.REST_API_SERVICE +"/posts/"+id;
    return this.httpClient.put(url,data,this.httpOptions).pipe()
  }
  deletePost(id): Observable<any> {
    let url = this.REST_API_SERVICE +"/posts/"+id;
    return this.httpClient.delete(url,this.httpOptions).pipe()
  }
}
