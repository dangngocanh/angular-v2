import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { DirectivesModule } from "./directives/directives.module";
import { MaterialModule } from "./material/meterial";

@NgModule({
    imports: [
      CommonModule,
      DirectivesModule,
      MaterialModule
    ],
    exports: [
      DirectivesModule,
      MaterialModule
    ],
    declarations: []
})
export class SharedModule{}
