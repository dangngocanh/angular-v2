import { Directive, OnInit, Input, Output, EventEmitter, ElementRef } from '@angular/core';


// set global $
declare const $: any;

@Directive({
  selector: '[uploadImage]'
})
export class UploadFileDirective implements OnInit {

  @Input('accept')
  accept: any;



  @Output('onComplete')
  onComplete = new EventEmitter();
  @Output('onError')
  onError = new EventEmitter();

  constructor(private el: ElementRef) {}

  ngOnInit() {
    const $this = this;

  }


}
