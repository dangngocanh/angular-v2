import { NgModule } from "@angular/core";
import { SearchFormComponent } from './search-form/search-form.component';
import { UploadFileDirective } from "./upload-file/upload-file.directive";
import { FormsModule } from '@angular/forms';
import { CommonModule } from "@angular/common";
@NgModule({
    imports: [
      FormsModule,
      CommonModule
    ],
    exports: [
      SearchFormComponent,
      UploadFileDirective
    ],
    declarations: [
    SearchFormComponent,
    UploadFileDirective
  ]
})
export class DirectivesModule{}
