import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { SidepanelService } from './api/side-panel.service';
import { LayoutModule } from './layout/layout.module';
import { ProgressBarModule } from './layout/components/progress-bar/progress-bar.module';
import { AppRoutingModule } from './app-routing.module';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,

    ProgressBarModule,
    LayoutModule
  ],
  providers: [SidepanelService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
