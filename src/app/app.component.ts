import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component } from '@angular/core';
import { Subscription } from 'rxjs';
import { HttpServerService } from './api/http-server.service';
import { SidepanelService } from './api/side-panel.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger(
      'slideBar', [
        state('collapse', style({
          width: 0,
          opacity: 0
        })),
        state('thin', style({
          width: '100%',
          opacity: 1
        })),
        state('full', style({
          width: '100%',
          opacity: 1
        })),
        transition('collapse => thin', animate('100ms ease-in')),
        transition('thin => full', animate('100ms ease-in')),
        transition('full => collapse', animate('100ms ease-out'))
      ]
    )
  ]
})
export class AppComponent {
  title = 'angular-v2';
  demo;
  state: string;
  private sub: Subscription;
  private themeSub: Subscription;

  constructor(hello: HttpServerService, public sidepanel: SidepanelService) {
    this.demo = hello.getProfile().subscribe((e) => {
      console.log(e);
    });
    hello.getList().subscribe((e) => {
      console.log(e);
    });
    // hello.addPost({
    //     title: "hello",
    //     author: "typicode"
    // }).subscribe(e => {
    //   console.log("add ",e);

    // })
    hello
      .updatePost(
        {
          title: 'test',
          author: 'typicode',
        },
        1
      )
      .subscribe((e) => {
        console.log('update', e);
      });
    // hello.deletePost(3).subscribe()
  }
  ngOnInit() {
    console.log("khởi tạo",this.themeSub);
    this.sub = this.sidepanel.state$.subscribe((s: string) => this.state = s);
    // this.themeSub = this.theme.theme$.subscribe((t: Theme) => this.themeClass = t.name);
  }

  ngOnDestroy() {
    this.demo.unsubscribe();
    console.log("hủy",this.themeSub);
    this.sub && this.sub.unsubscribe();
    this.themeSub && this.themeSub.unsubscribe();
  }
}
